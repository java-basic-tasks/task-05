package com.epam.rd.java.basic.task05;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.lang.reflect.Method;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import com.epam.rd.java.basic.TestHelper;

public class TreeTest {
	
	private static final PrintStream STD_OUT = System.out;

	private final ByteArrayOutputStream baos = new ByteArrayOutputStream();

	private final PrintStream out = new PrintStream(baos);
	
	@BeforeEach
	void setUpControlledOut() {
		System.setOut(out);
	}

	@AfterEach void tearDownControlledOut() {
		System.setOut(STD_OUT);
	}

	@SuppressWarnings("unchecked")
	@ParameterizedTest
	@CsvFileSource(resources = "data.csv", delimiter = ';')
	void test(String actionsData, String expectedTree) throws Exception {
		String expected = expectedTree.replaceAll("~", System.lineSeparator());

		Tree<Integer> tree = Tree.class.getConstructor().newInstance();
		for (String action : actionsData.split("\\s+")) {
			String actionName = actionResolver(action.charAt(0));
			Method actionMethod = Tree.class.getMethod(actionName, Comparable.class);

			String actionValues = action.substring(action.indexOf('-') + 1); 
			for (String value : actionValues.split("-")) {
				actionMethod.invoke(tree, new Object[] { Integer.parseInt(value) });
			}
		}

		System.out.println();
		tree.print();
		out.flush();
		String actual = baos.toString();

		// Set TestHelper.IS_LOG_ON=true to turn on logging of details
		TestHelper.logToStdErr(expected, actual);

		assertEquals(expected, actual);
	}
	
	private static String actionResolver(char actionChar) {
		switch (actionChar) {
		case 'a':
			return "add";
		case 'r':
			return "remove";
		default:
			return null;
		}
	}

}

