package com.epam.rd.java.basic.task05;

public class Tree<E extends Comparable<E>> {

	private static final String INDENT = "  ";

	public Tree() {
	}

	private static final class Node<E extends Comparable<E>> {
	}

	public void add(E[] ar) {
	}

	public boolean add(E e) {
		return false;
	}

	private boolean add(Node<E> node, E e) {
		return false;
	}

	public void print() {
	}

	public boolean remove(E e) {
		return false;
	}

}