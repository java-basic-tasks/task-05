package com.epam.rd.java.basic.task05;

public class Task {
	
	public static void main(String[] args) {
		Tree<Integer> tree = new Tree<>();
		tree.add(new Integer[] {3, 1, 2, 6, 4, 7, 0, 5});
		tree.print();
		System.out.println("~~~~~~~~~~~~~~");

		for (int j : new int[] {3, 5, 4, 1, 0} ) {
			System.out.printf("remove %d: %b%n", j, tree.remove(j));
			System.out.println("~~~~~~~~~~~~~~");
			tree.print();
			System.out.println("~~~~~~~~~~~~~~");
		}
	}

}
